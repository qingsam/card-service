package handler

/**
 * Contains the logic to handle incoming request from merchants
 */
import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"strconv"

	mdl "bitbucket.org/qingsam/card-service/model"
	repo "bitbucket.org/qingsam/card-service/repository"
	"github.com/gorilla/mux"
)

func AuthRequest(w http.ResponseWriter, r *http.Request) {
	var reserveTrans, err = parseToReservedTrans(r.Body)
	if err != nil {
		panic(err)
	}
	vars := mux.Vars(r)
	merchantID, err := strconv.Atoi(vars["id"])
	if err != nil {
		http.Error(w, "merchant id required", http.StatusBadRequest)
		return
	}
	if reserveTrans.AccountID == 0 {
		http.Error(w, "accountID required", http.StatusBadRequest)
		return
	}
	if reserveTrans.MerchantID != merchantID {
		http.Error(w, "merchantID in body does not match param ID", http.StatusBadRequest)
		return
	}
	log.Print(reserveTrans)
	trans, err := repo.Repository.AddReserveTrans(reserveTrans.AccountID, merchantID, reserveTrans.ReservedAmount)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	if err := json.NewEncoder(w).Encode(trans); err != nil {
		panic(err)
	}
}

func CaptureTransaction(w http.ResponseWriter, r *http.Request) {

	var reserveTrans, err = parseToReservedTrans(r.Body)
	if err != nil {
		panic(err)
	}
	vars := mux.Vars(r)
	merchantID, err := strconv.Atoi(vars["id"])
	if err != nil {
		http.Error(w, "merchant id required", http.StatusBadRequest)
		return
	}
	if reserveTrans.MerchantID != merchantID {
		http.Error(w, "merchantID in body does not match param ID", http.StatusBadRequest)
		return
	}
	if reserveTrans.AccountID == 0 || reserveTrans.MerchantID == 0 || reserveTrans.ReservedTransID == 0 {
		http.Error(w, "make sure you have the following:  AccountID, merchantID and ReservedTransID", http.StatusBadRequest)
		return
	}
	capturedTrans, err := repo.Repository.CaptureReserveTrans(reserveTrans.ReservedTransID, reserveTrans.MerchantID, reserveTrans.AccountID, reserveTrans.ReservedAmount)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	if err := json.NewEncoder(w).Encode(capturedTrans); err != nil {
		panic(err)
	}
}

func ReverseTransaction(w http.ResponseWriter, r *http.Request) {
	var reserveTrans, err = parseToReservedTrans(r.Body)
	if err != nil {
		panic(err)
	}
	vars := mux.Vars(r)
	merchantID, _ := strconv.Atoi(vars["id"])
	if reserveTrans.MerchantID != merchantID {
		http.Error(w, "merchantID in body does not match param ID", http.StatusBadRequest)
		return
	}
	isReversed, err := repo.Repository.ReverseReserveTrans(reserveTrans.ReservedTransID, reserveTrans.MerchantID, reserveTrans.AccountID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	fmt.Fprint(w, `{"reverse-state":`, isReversed, "}")
}

func Refund(w http.ResponseWriter, r *http.Request) {
	var refundTrans, err = parseToRefundTrans(r.Body)
	if err != nil {
		panic(err)
	}
	vars := mux.Vars(r)
	merchantID, _ := strconv.Atoi(vars["id"])
	if refundTrans.MerchantID != merchantID {
		http.Error(w, "merchantID in body does not match param ID", http.StatusBadRequest)
		return
	}
	if refundTrans.AccountID == 0 || refundTrans.MerchantID == 0 || refundTrans.RefundAmount == 0 {
		http.Error(w, "make sure you have the following:  AccountID, merchantID and ReservedTransID", http.StatusBadRequest)
		return
	}
	log.Print(&refundTrans.AccountID)
	isRefund, err := repo.Repository.Refund(&refundTrans)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	fmt.Fprint(w, `{"refund-state":`, isRefund, "}")
}

func parseToRefundTrans(r io.Reader) (mdl.RefundTrans, error) {
	decoder := json.NewDecoder(r)
	var refundTrans mdl.RefundTrans
	err := decoder.Decode(&refundTrans)
	return refundTrans, err
}

func parseToReservedTrans(r io.Reader) (mdl.ReservedTrans, error) {
	decoder := json.NewDecoder(r)
	var reserveTrans mdl.ReservedTrans
	err := decoder.Decode(&reserveTrans)
	return reserveTrans, err
}
