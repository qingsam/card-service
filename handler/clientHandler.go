package handler

/**
 * Contains the logic to handle incoming request from account holders
 */
import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"

	mdl "bitbucket.org/qingsam/card-service/model"
	repo "bitbucket.org/qingsam/card-service/repository"
	"github.com/gorilla/mux"
)

// CreateAccount create an account given a name
func CreateAccount(w http.ResponseWriter, r *http.Request) {
	name := r.FormValue("name")
	if name == "" {
		http.Error(w, "create account missing name query param", http.StatusBadRequest)
		return
	}
	accountCreated := repo.Repository.Create(name)
	log.Printf("craeteing account for %s", name)
	if err := json.NewEncoder(w).Encode(accountCreated); err != nil {
		panic(err)
	}

}

// Deposit handles the deposit of money
func Deposit(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	decoder := json.NewDecoder(r.Body)
	var t mdl.Deposit
	err := decoder.Decode(&t)
	if err != nil {
		panic(err)
	}
	log.Println(t.Amount)

	accountID, err := strconv.Atoi(vars["id"])
	if err != nil {
		http.Error(w, "missing accountID", http.StatusBadRequest)
		return
	}

	depositSucess, err := repo.Repository.Deposit(accountID, t.Amount)
	if err != nil {
		log.Print(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	fmt.Fprint(w, `{"deposit-state":`, depositSucess, "}")
}

// Balance shows the amount a account has
func Balance(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	accountID, err := strconv.Atoi(vars["id"])
	if err != nil {
		http.Error(w, "missing accountID", http.StatusBadRequest)
		return
	}

	balance, err := repo.Repository.Balance(accountID)
	if err != nil {
		log.Print(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	fmt.Fprint(w, `{"balance":`, balance, "}")
}

func Statement(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	accountID, err := strconv.Atoi(vars["id"])
	if err != nil {
		http.Error(w, "missing accountID", http.StatusBadRequest)
		return
	}
	statement, err := repo.Repository.Statement(accountID)
	log.Print(statement)
	if err := json.NewEncoder(w).Encode(statement); err != nil {
		panic(err)
	}
}
