package main

import (
	"log"
	"net/http"

	pb "bitbucket.org/qingsam/card-service/routes"
)

func main() {
	router := pb.NewRouter()
	log.Println("running on port 8080")
	log.Fatal(http.ListenAndServe("0.0.0.0:8080", router))
}
