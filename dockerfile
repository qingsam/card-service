FROM golang:latest
WORKDIR /app
ENV SRC_DIR=/go/src/bitbucket.org/qingsam/card-service
ADD . $SRC_DIR
RUN go get -d github.com/gorilla/mux
RUN cd $SRC_DIR; go build -o myapp; cp myapp /app/
EXPOSE 8080
ENTRYPOINT ["./myapp"]

