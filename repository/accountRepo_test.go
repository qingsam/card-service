package repository

import (
	"testing"

	mdl "bitbucket.org/qingsam/card-service/model"
)

func TestCreate(t *testing.T) {
	repo := AccountRepo{
		Db: make(map[int]*mdl.Account),
	}
	// Create account
	accountName := "Me"
	account := repo.Create(accountName)

	if account.Name != accountName {
		t.Errorf("account name was incorrect, got: %s, want: %s.", account.Name, accountName)
	}

	balance, _ := repo.Balance(account.AccountID)
	if balance != 0 {
		t.Errorf("balance was incorrect, got: %f, want: %d.", balance, 0)
	}
}

func TestDeposit(t *testing.T) {
	repo := AccountRepo{
		Db: make(map[int]*mdl.Account),
	}
	// Create account
	accountName := "Me"
	account := repo.Create(accountName)

	// deposit some money into account
	repo.Deposit(account.AccountID, 300.00)

	balance, _ := repo.Balance(account.AccountID)
	if balance != 300.00 {
		t.Errorf("balance was incorrect, got: %f, want: %f.", balance, 300.00)
	}
}

func TestAddReserveTrans(t *testing.T) {
	repo := AccountRepo{
		Db: make(map[int]*mdl.Account),
	}
	// Create account
	accountName := "Me"
	account := repo.Create(accountName)
	// deposit some money into account
	repo.Deposit(account.AccountID, 300.00)

	// add reserve transaction
	addedReserveTrans, _ := repo.AddReserveTrans(account.AccountID, 19, 20.0)
	rsvedTrans := repo.Db[account.AccountID].ReservedTrans
	if rsvedTrans[addedReserveTrans.ReservedTransID].AccountID != account.AccountID {
		t.Errorf("account was incorrect, got: %d, want: %d.", rsvedTrans[0].AccountID, account.AccountID)
	}
	if rsvedTrans[addedReserveTrans.ReservedTransID].ReservedAmount != 20.0 {
		t.Errorf("ReservedAmount was incorrect, got: %f, want: %f.", rsvedTrans[0].ReservedAmount, 20.0)
	}
}

func TestCaptureReserveTrans(t *testing.T) {
	repo := AccountRepo{
		Db: make(map[int]*mdl.Account),
	}
	// Create account
	accountName := "Me"
	account := repo.Create(accountName)
	// deposit some money into account
	repo.Deposit(account.AccountID, 300.00)
	// add reserve transaction
	addedReserveTrans, _ := repo.AddReserveTrans(account.AccountID, 19, 20.0)
	repo.CaptureReserveTrans(addedReserveTrans.ReservedTransID, addedReserveTrans.MerchantID, addedReserveTrans.AccountID, 9.0)
	balance, _ := repo.Balance(account.AccountID)
	if balance != 291.00 {
		t.Errorf("balance was incorrect, got: %f, want: %f.", balance, 300.00)
	}
}

func TestReverseReserveTrans(t *testing.T) {
	repo := AccountRepo{
		Db: make(map[int]*mdl.Account),
	}
	// Create account
	accountName := "Me"
	account := repo.Create(accountName)
	// deposit some money into account
	repo.Deposit(account.AccountID, 300.00)
	// add reserve transaction
	addedReserveTrans, _ := repo.AddReserveTrans(account.AccountID, 19, 20.0)
	isReversed, _ := repo.ReverseReserveTrans(addedReserveTrans.ReservedTransID, addedReserveTrans.MerchantID, addedReserveTrans.AccountID)
	if !isReversed {
		t.Errorf("ReverseReserveTrans was unsuccessful")
	}
	if len(repo.Db[account.AccountID].ReservedTrans) != 0 {
		t.Errorf("length of ReservedTrans was incorrect, got: %d, want: %d.", len(repo.Db[account.AccountID].ReservedTrans), 0)
	}
}

func TestRefund(t *testing.T) {
	repo := AccountRepo{
		Db: make(map[int]*mdl.Account),
	}
	// Create account
	accountName := "Me"
	account := repo.Create(accountName)
	refund := mdl.RefundTrans{
		AccountID:    account.AccountID,
		MerchantID:   1,
		RefundAmount: 199.00,
	}
	repo.Refund(&refund)
	if repo.Db[account.AccountID].Balance != 199.0 {
		t.Errorf("lBalance was incorrect, got: %f, want: %f.", repo.Db[account.AccountID].Balance, 199.0)
	}
}
