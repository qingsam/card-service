package repository

import (
	"errors"
	"fmt"
	"log"

	mdl "bitbucket.org/qingsam/card-service/model"
)

// AccountRepo carry out reuqest on accounts
type AccountRepo struct {
	// mock db with hashmap
	Db map[int]*mdl.Account
}

// Create - process the creation of an card account
func (repo *AccountRepo) Create(accountName string) *mdl.Account {
	account := mdl.Account{Name: accountName}
	var accountID = len(repo.Db) + 1*100 // create random account ID starting from 100
	account.AccountID = accountID
	account.ReservedTrans = make(map[int]*mdl.ReservedTrans)

	log.Printf("creating account for %d", account.AccountID)
	repo.Db[accountID] = &account
	return &account
}

// Deposit - deposit money into the account
func (repo *AccountRepo) Deposit(accountID int, amount float32) (bool, error) {
	log.Printf("deposit £%f for %d", amount, accountID)
	return repo.creditAccount(accountID, amount)
}

// Balance - gets the balance for a specified account
func (repo *AccountRepo) Balance(accountID int) (float32, error) {
	depositAccount, exist := repo.Db[accountID]
	if !exist {
		return 0, fmt.Errorf("no account: %d", accountID)
	}
	return depositAccount.Balance, nil
}

// Statement - creates a statement for the account
func (repo *AccountRepo) Statement(accountID int) (*mdl.Account, error) {
	theAccount, exist := repo.Db[accountID]
	if !exist {
		return theAccount, fmt.Errorf("no account: %d", accountID)
	}
	return theAccount, nil
}

// AddReserveTrans -  reserve money for a merchant to capture
func (repo *AccountRepo) AddReserveTrans(accountID int, merchantID int, reservedAmount float32) (*mdl.ReservedTrans, error) {
	rt := mdl.ReservedTrans{
		AccountID:      accountID,
		MerchantID:     merchantID,
		ReservedAmount: reservedAmount,
	}
	theAccount, exist := repo.Db[rt.AccountID]
	if !exist {
		return nil, fmt.Errorf("no account: %d", rt.AccountID)
	}
	if !repo.hasSufficientFunds(theAccount, rt.ReservedAmount) {
		return nil, errors.New("insufficient funds in account")
	}
	rt.ReservedTransID = len(theAccount.ReservedTrans) + 1
	theAccount.ReservedTrans[rt.ReservedTransID] = &rt
	return &rt, nil
}

// CaptureReserveTrans - money to capture
func (repo *AccountRepo) CaptureReserveTrans(reservedTransID int, merchantID int, accountID int, reservedAmount float32) (*mdl.ReservedTrans, error) {
	captureTrans := mdl.ReservedTrans{
		ReservedTransID: reservedTransID,
		AccountID:       accountID,
		MerchantID:      merchantID,
		ReservedAmount:  reservedAmount,
	}
	theAccount, exist := repo.Db[captureTrans.AccountID]
	if !exist {
		return nil, fmt.Errorf("no account: %d", captureTrans.AccountID)
	}
	theReservedTrans, exist := theAccount.ReservedTrans[captureTrans.ReservedTransID]
	if !exist {
		return nil, errors.New("invalid ReservedTransID")
	}

	if captureTrans.ReservedAmount > theReservedTrans.ReservedAmount {
		return nil, errors.New("the ammount you exceed the capture limit")
	}

	theAccount.Balance = theAccount.Balance - captureTrans.ReservedAmount // minus from balance

	// re calc capture balance
	amountLeftToCapture := theReservedTrans.ReservedAmount - captureTrans.ReservedAmount
	theReservedTrans.ReservedAmount = amountLeftToCapture
	if amountLeftToCapture == 0 {
		delete(theAccount.ReservedTrans, captureTrans.ReservedTransID)
	}
	log.Printf("processing money for merchant %d of %f", captureTrans.MerchantID, captureTrans.ReservedAmount)
	return theReservedTrans, nil
}

// ReverseReserveTrans -  reverse a reserved transation
func (repo *AccountRepo) ReverseReserveTrans(reservedTransID int, merchantID int, accountID int) (bool, error) {
	return repo.removeFromReservedTrans(reservedTransID, merchantID, accountID)
}

// Refund - refund an account
func (repo *AccountRepo) Refund(refundTrans *mdl.RefundTrans) (bool, error) {
	log.Printf("refund %d for %f", refundTrans.AccountID, refundTrans.RefundAmount)
	return repo.creditAccount(refundTrans.AccountID, refundTrans.RefundAmount)
}

func (repo *AccountRepo) removeFromReservedTrans(reservedTransID int, merchantID int, accountID int) (bool, error) {
	theAccount, exist := repo.Db[accountID]
	if !exist {
		return false, errors.New("no account id")
	}
	_, ok := theAccount.ReservedTrans[reservedTransID]
	if !ok {
		return false, errors.New("invalid reserved transaction ID")
	}
	delete(theAccount.ReservedTrans, reservedTransID)
	return true, nil
}

// this methods is used to credit the account (e.g. deposit and refund)
func (repo *AccountRepo) creditAccount(accountID int, amount float32) (bool, error) {
	depositAccount, exist := repo.Db[accountID]
	if !exist {
		return false, fmt.Errorf("no account: %d", accountID)
	}
	depositAccount.Balance = depositAccount.Balance + amount
	return true, nil
}

func (repo *AccountRepo) hasSufficientFunds(theAccount *mdl.Account, amountToProcess float32) bool {
	return (theAccount.Balance - amountToProcess) > 0
}

// Repository mocking the db
var Repository = AccountRepo{
	Db: make(map[int]*mdl.Account),
}
