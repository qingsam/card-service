package model

// ReservedTrans the transations that is reserved through auth-request
type ReservedTrans struct {
	ReservedTransID int
	AccountID       int
	MerchantID      int
	ReservedAmount  float32
}
