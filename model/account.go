package model

// Account holds the account details
// AccountID the account id
// Name the name of the card holder
// Balance the balance on the card
// ReservedTrans the reserved transaction for the card (could be a different service)
type Account struct {
	AccountID     int     `json:"accountID"`
	Name          string  `json:"name"`
	Balance       float32 `json:"balance"`
	ReservedTrans map[int]*ReservedTrans
}

// Desposit holds the amount to deposit
type Deposit struct {
	Amount float32
}
