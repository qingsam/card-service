package model

// RefundTrans struct containing the attr to process a refund
// AccountID the account ID
// MerchantID the Merchant ID
// RefundAmount the amount to be refunded
type RefundTrans struct {
	AccountID    int
	MerchantID   int
	RefundAmount float32
}
