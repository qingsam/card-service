# Running the app
- docker build -t card-service:1.0.0 .
- docker run --rm -p 8080:8080 card-service:1.0.0

# Create an account
curl -X POST \
  'http://localhost:8080/account/create?name=qing'

# Deposit money to accountId 100 of �3000
curl -X POST \
  http://localhost:8080/account/100/deposit \
  -d '{
	"Amount": 3000
}'

# Check balance for the accountId 100
curl -X GET \
  http://localhost:8080/account/100/balance

# Purchase something from multi merchants with the account 100
curl -X POST \
  http://localhost:8080/merchant/29/auth-request \
  -d '{
	"AccountID": 100,
	"MarchantID":29,
	"ReservedAmount": 80
}'

curl -X POST \
  http://localhost:8080/merchant/11/auth-request \
  -d '{
	"AccountID": 100,
	"MarchantID":11,
	"ReservedAmount": 50
}'

# View statement
curl -X GET \
  http://localhost:8080/account/100/statement

# capture more than requested
curl -X POST \
  http://localhost:8080/merchant/19/capture \
  -d '{
	"AccountID": 100,
	"ReservedTransID": 2,
	"MarchantID": 19,
	"ReservedAmount": 100
}'

# capture requested
curl -X POST \
  http://localhost:8080/merchant/19/capture \
  -d '{
	"AccountID": 100,
	"ReservedTransID": 2,
	"MarchantID": 19,
	"ReservedAmount": 10
}'

curl -X POST \
  http://localhost:8080/merchant/19/capture \
  -d '{
	"AccountID": 100,
	"ReservedTransID": 2,
	"MarchantID": 19,
	"ReservedAmount": 40
}'

# View statement - should now contain �2950
curl -X GET \
  http://localhost:8080/account/100/statement

# Reverse transaction
curl -X POST \
  http://localhost:8080/merchant/19/reverse \
  -d '{
	"AccountID": 100,
	"ReservedTransID": 1,
	"MarchantID": 1
}'

# Refund transaction
curl -X POST \
  http://localhost:8080/merchant/19/refund \
  -d '{
	"AccountID": 100,
	"MarchantID": 19,
	"RefundAmount": 100
}'