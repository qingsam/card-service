package routes

import (
	"net/http"

	hdl "bitbucket.org/qingsam/card-service/handler"
	"github.com/gorilla/mux"
)

type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

type Routes []Route

// NewRouter gg
func NewRouter() *mux.Router {
	router := mux.NewRouter().StrictSlash(true)
	for _, route := range routes {
		router.
			Methods(route.Method).
			Path(route.Pattern).
			Name(route.Name).
			Handler(route.HandlerFunc)
	}
	return router
}

var routes = Routes{
	Route{
		"Create",
		"POST",
		"/account/create",
		hdl.CreateAccount,
	},
	Route{
		"Deposit",
		"POST",
		"/account/{id:[0-9]+}/deposit",
		hdl.Deposit,
	},
	Route{
		"Balance",
		"GET",
		"/account/{id:[0-9]+}/balance",
		hdl.Balance,
	},
	Route{
		"Statement",
		"GET",
		"/account/{id:[0-9]+}/statement",
		hdl.Statement,
	},
	Route{
		"Auth-Req",
		"POST",
		"/merchant/{id:[0-9]+}/auth-request",
		hdl.AuthRequest,
	},
	Route{
		"Capture",
		"POST",
		"/merchant/{id:[0-9]+}/capture",
		hdl.CaptureTransaction,
	},
	Route{
		"Reverse",
		"POST",
		"/merchant/{id:[0-9]+}/reverse",
		hdl.ReverseTransaction,
	},
	Route{
		"Refund",
		"POST",
		"/merchant/{id:[0-9]+}/refund",
		hdl.Refund,
	},
}
